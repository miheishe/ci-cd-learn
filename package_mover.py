import subprocess
import argparse
import os


def get_package_versions(package_name, creds, only_last_version=False):
    output = subprocess.check_output(['pip', 'index', 'versions', package_name, '--index-url', creds]).decode('utf-8')
    versions = output.strip().split(': ')[1].split(', ')
    if only_last_version:
        return versions[0]
    else:
        return versions


def process_requirements_file(filename, creds):
    packages = []
    with open(filename, 'r') as file:
        for line in file:
            line = line.strip()
            if line.startswith('#'):
                continue
            if '#' in line:
                line = line.split('#', 1)[0].strip()
            if '==' in line:
                package, version_spec = line.split('==', 1)
                versions = []
                if version_spec == '*':
                    versions = get_package_versions(package, creds)
                elif ',' in version_spec:
                    version_list = version_spec.split(',')
                    for version in version_list:
                        versions.append(version.strip())
                else:
                    versions.append(version_spec.strip())
                for item in versions:
                    packages.append(f'{package}=={item}')
            else:
                packages.append(f'{line}=={get_package_versions(line, creds, only_last_version=True)}')
    return packages


def url_splitter(url):
    splited = url.split('://')
    if (splited[0] != 'https') and (splited[0] != 'http'):
        splited = ['https'] + splited
    return splited


def arguments_parser(parser):
    parser.add_argument('--from_url',
                        help='Адрес репозитория "отправления". '
                             'Если не указан "http(s)://" по умолчанию будет добавлен https')
    parser.add_argument('--from_user',
                        help='Логин репозитория "отправления". '
                             'Если не указан, считается что авторизация не требуется')
    parser.add_argument('--from_pass',
                        help='Пароль репозитория "отправления". '
                             'Если не указан, считается что авторизация не требуется')
    parser.add_argument('--to_url',
                        help='Адрес репозитория "назначения". '
                             'Если не указан "http(s)://" по умолчанию будет добавлен https')
    parser.add_argument('--to_user',
                        help='Логин репозитория "назначения". '
                             'Если не указан, считается что авторизация не требуется')
    parser.add_argument('--to_pass',
                        help='Пароль репозитория "назначения". '
                             'Если не указан, считается что авторизация не требуется')

    args = parser.parse_args()

    if (args.from_url is None) and (args.to_url is None):
        print('Не указаны адреса репозиториев!')
        exit(1)

    if (args.from_user is None) or (args.from_pass is None):
        from_login_pass_url = args.from_url
    else:
        from_login_pass_url = \
            f'{url_splitter(args.from_url)[0]}://{args.from_user}:{args.from_pass}@{url_splitter(args.from_url)[1]}'

    if (args.to_user is None) or (args.to_pass is None):
        to_login_pass_url = args.to_url
    else:
        to_login_pass_url = \
            f'{url_splitter(args.to_url)[0]}://{args.to_user}:{args.to_pass}@{url_splitter(args.to_url)[1]}'

    cred_from = {'simple_auth': from_login_pass_url,
                 'username': args.from_user,
                 'password': args.from_pass,
                 'host': url_splitter(args.from_url)[1].split("/")[0],
                 'repo_url': args.from_url}

    cred_to = {'simple_auth': to_login_pass_url,
               'username': args.to_user,
               'password': args.to_pass,
               'host': url_splitter(args.to_url)[1].split("/")[0],
               'repo_url': args.to_url}

    return cred_from, cred_to


def package_downloader(list_of_packages, creds):
    for package in list_of_packages:
        package_dir = 'downloads/' + '-'.join(package.split('=='))
        os.makedirs(package_dir, exist_ok=True)

        print("Загрузка пакета в формате wheel")
        subprocess.run(['pip', 'download', package, '--index-url', creds['simple_auth'], '--trusted-host',
                        creds['host'], '--no-dependencies', '--dest', package_dir], capture_output=True, text=True)

        print("Загрузка пакета в формате tar.gz")
        subprocess.run(['pip', 'download', package, '--extra-index-url', creds['simple_auth'], '--trusted-host',
                        creds['host'], '--no-dependencies', '--no-binary', ':all:', '--dest', package_dir])


def packages_uploader(files_path, creds):
    print(subprocess.run(['twine', 'upload', '--repository-url', creds['repo_url'],
                          '--username', creds['username'], '--password', creds['password'],
                          files_path, '--skip-existing', '--non-interactive', '--disable-progress-bar'],
                         capture_output=True, text=True))


def main():
    parser = argparse.ArgumentParser()
    cred_from, cred_to = arguments_parser(parser)
    packages = process_requirements_file(filename='package_list.txt', creds=cred_from['simple_auth'])
    package_downloader(packages, creds=cred_from)
    packages_uploader(files_path='./downloads/*/*', creds=cred_to)


if __name__ == '__main__':
    main()
