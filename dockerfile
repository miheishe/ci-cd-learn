FROM python:3.8
WORKDIR /work
RUN mkdir "/work/downloads" && \
    apt-get update && \
    apt-get install -y curl gcc cmake make -y && \
    apt autoremove -y && \
    python -m pip install --upgrade pip && \
    pip install twine && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --profile minimal -y
ENV PATH="/root/.cargo/bin:$PATH"
RUN rustup update stable
COPY ./package_mover.py /work/

ENTRYPOINT ["python", "/work/package_mover.py"]
# Для успешного запуска файл package_list.txt должен быть примонтирован в папку /work